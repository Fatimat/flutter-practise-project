import 'package:flutter/material.dart';
import './random_words.dart';

void main() => runApp(MyApp());
// Basic
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // Similar to const in JS
//     final wordPair = WordPair.random();
//     return MaterialApp(
//         theme: ThemeData(primaryColor: Colors.purple),
//         home:
//         Scaffold(
//             appBar: AppBar(title: Text('WordPair Generator')),
//             body: Center(child: Text(wordPair.asPascalCase))));
//   }
// }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primaryColor: Colors.purple), home: RandomWords());
  }
}
